(function(){
  const currentPath = window.location.pathname,
        dataURLKey  = currentPath+'dataURL',
        opacityKey  = currentPath+'opacity',
        initKey     = currentPath+'init';
  const DEFAULT_OPACITY = 0.8;
  class PixelImage {
    constructor(dataURL) {
      this.dataURL = dataURL;
      this.opacity = parseFloat(window.localStorage[opacityKey]);
      this.element = null;
    }
    create() {
      let img = document.createElement('img');
      img.src = this.dataURL;
      this.element = document.createElement('div');
      if(!this.opacity && this.opacity !== 0) {
        this.opacity = DEFAULT_OPACITY;
      }
      saveImageData(this.dataURL, this.opacity);
      merge(this.element.style, styles.image);
      img.onload = () => {
        merge(this.element.style, {
          height: img.height + 'px',
          background: `url(${this.dataURL}) no-repeat top center`,
          opacity: this.opacity
        });
      }
    }
    updateImage(dataURL) {
      let img = document.createElement('img');
      img.src = dataURL;
      this.dataURL = dataURL;
      this.element.style.backgroundImage = `url(${dataURL})`;
      this.element.style.opacity = DEFAULT_OPACITY;
      img.onload = () => {
        this.element.style.height = img.height + 'px';
      }
      saveImageData(dataURL, DEFAULT_OPACITY);
    }
    updateOpacity(opacity) {
      this.element.style.opacity = opacity;
      this.opacity = opacity;
      saveImageData(this.dataURL, opacity);
    }
    appendToBody() {
      const body = document.querySelector('body');
      body.appendChild(this.element);
    }
    remove() {
      const body = document.querySelector('body');
      body.removeChild(this.element);
    }
  }

  class PixelControl {
    constructor() {
      this.currentImage = null;
      this.element = null;
      this.openElement = null;
      this.increaseOpacityBtn = null;
      this.reduceOpacityBtn = null;
      this.toggleElement = null;
    }
    init() {
      const dataURL = window.localStorage[dataURLKey];
      this.create();
      if(dataURL) {
        this.bindImage(dataURL);
      }
      this.handleReduceOpacity();
      this.handleIncreaseOpacity();
      this.handleImageToggle();
    }
    create() {
      const body = document.querySelector('body');
      if(this.element) {
        body.removeChild(this.element);
      }
      this.element = document.createElement('div');
      this.openElement = document.createElement('input');
      this.increaseOpacityBtn = document.createElement('a');
      this.reduceOpacityBtn = document.createElement('a');
      this.toggleElement = document.createElement('a');
      this.openElement.type = 'file';
      this.increaseOpacityBtn.innerText = '+';
      this.reduceOpacityBtn.innerText = '-';
      this.toggleElement.innerText = '____)';
      merge(this.element.style, styles.controlContainer);
      merge(this.openElement.style, styles.openElement);
      merge(this.increaseOpacityBtn.style, styles.increaseOpacity);
      merge(this.reduceOpacityBtn.style, styles.reduceOpacity);
      merge(this.toggleElement.style, styles.toggleElement);
      this.element.appendChild(this.openElement);
      this.element.appendChild(this.increaseOpacityBtn);
      this.element.appendChild(this.reduceOpacityBtn);
      this.element.appendChild(this.toggleElement);

      body.appendChild(this.element);
    }
    open() {
      this.openElement.addEventListener('change', handleFileSelect);
    }
    remove() {
      if(this.element) {
        this.element.style.display = 'none';
        this.currentImage.element.style.display = 'none';
      }
    }
    bindImage(dataURL) {
      if(!this.currentImage){
        this.currentImage = new PixelImage(dataURL);
        this.currentImage.create();
        this.currentImage.appendToBody();
      } else {
        this.currentImage.updateImage(dataURL);
      }
    }
    handleReduceOpacity() {
      this.reduceOpacityBtn.addEventListener('click', (e) => {
        e.preventDefault();

        let opacity = getOpacity();
        opacity -= 0.1;
        opacity = correctOpacity(opacity);

        this.currentImage.updateOpacity(opacity);
      });
    }
    handleIncreaseOpacity() {
      this.increaseOpacityBtn.addEventListener('click', (e) => {
        e.preventDefault();
        let opacity = getOpacity();
        opacity += 0.1;
        opacity = correctOpacity(opacity);
        this.currentImage.updateOpacity(opacity);
      });
    }
    handleImageToggle() {
      this.toggleElement.addEventListener('click', (e) => {
        e.preventDefault();
        if(getOpacity()) {
          this.currentImage.updateOpacity(0);
        } else {
          this.currentImage.updateOpacity(1);
        }
      });
    }
  }

  let imageControl = new PixelControl();
  function checkInitialization() {
    let initialized = parseInt(window.localStorage[initKey]);
    if(window.location.hash == '#pixel') {
      if(initialized) {
        initialized = 0;
        window.localStorage[initKey] = 0;
      } else {
        initialized = 1;
        window.localStorage[initKey] = 1;
      }
    }
    return initialized;
  }
  function pixelInit() {
    if(checkInitialization()) {
      imageControl.init();
      imageControl.open();
    } else {
      imageControl.remove();
    }
  }
  document.addEventListener("DOMContentLoaded", pixelInit);
  window.onhashchange = pixelInit;

  function merge(dst, src) {
    for(let key in src) {
      dst[key] = src[key];
    }
  }

  function handleFileSelect(e) {
    const file = e.target.files[0];
    let reader = new FileReader();
    reader.onload = function() {
      const dataURL = reader.result;
      imageControl.bindImage(dataURL);
    };
    reader.readAsDataURL(file);
  }

  function saveImageData(dataURL, opacity) {
    window.localStorage[dataURLKey] = dataURL;
    window.localStorage[opacityKey] = opacity;
  }

  function getOpacity() {
    return parseFloat(window.localStorage[opacityKey]);
  }

  function correctOpacity(opacity) {
    if(opacity < 0) {
      opacity = 0;
    }
    if(opacity > 1) {
      opacity = 1;
    }
    return opacity;
  }
  const styles = {
    image: {
      width: '100%',
      position: 'absolute',
      zIndex: '2147483646',
      top: 0,
      left: 0,
      pointerEvents: 'none'
    },
    controlContainer: {
      backgroundColor: '#000',
      position: 'fixed',
      right: 0,
      top: '50%',
      width: '80px',
      zIndex: '2147483647'
    },
    openElement: {
      width: '80px'
    },
    increaseOpacity: {
      height: '20px',
      width: '40px',
      background: '#000',
      color: '#fff',
      display: 'inline-block',
      textAlign: 'center',
      cursor: 'pointer'
    },
    reduceOpacity: {
      height: '20px',
      width: '40px',
      background: '#000',
      color: '#fff',
      display: 'inline-block',
      textAlign: 'center',
      cursor: 'pointer'
    },
    toggleElement: {
      height: '20px',
      width: '80px',
      background: '#000',
      color: '#fff',
      display: 'inline-block',
      textAlign: 'center',
      cursor: 'pointer'
    }
  };
})();
