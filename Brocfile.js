var jshintTrees = require('broccoli-jshint'),
    Funnel      = require('broccoli-funnel'),
    concat      = require('broccoli-concat'),
    babel       = require('broccoli-babel-transpiler'),
    mergeTrees  = require('broccoli-merge-trees');

var app = {
  'js':     'js',
  'mainJS': 'pixel-perfect.js',
  'public': 'public'
};


var jshint = jshintTrees(app.js, {
  disableTestGenerator: true,
});

staticFiles = new Funnel(app.public,{
  destDir: ''
});

var scriptFiles = babel(new Funnel(app.js, {include: ['**/*.js']}), {
  stage: 0,
  browserPolyfill: true,
  filterExtensions:['js'],
});

scriptFiles = concat(scriptFiles, {
  inputFiles: ['**/*.js'],
  outputFile: app.mainJS
});

module.exports = mergeTrees([scriptFiles, jshint, staticFiles]);
